# ENTIDADES:
    ## 1. Protein: 
        - protein_name[String][required] -> Ex. 2ki5
        - protein_composition[select: simple (aminoacid), conjugated][required]
        - protein_type[radiobutton: animal, vegetable][optinal]
        - protein_function[checkbox: hormonal, enzymatic, structural, defensive, storage, conveyor, receiver, motor][optional]
    ## 2. Chain:
        - chain_name[String][required] -> Ex. A
        - chain_sequence(aminoacidos de 1 letra)[pattern="ARNDCQEGHILKMFPSTWYV"][String][required] -> Ex. ARRRACDFFFAPYCCCDQE
        - chain_sequence_length[number][required] -> Ex. 366 (se rellena automaticamente a partir de chain_sequence)

    ## 2. Aminoacid: 
        - residue_name[select: Alanine, Arginine, Asparagine, Aspartic acid, Cysteine, Glutamine, Glutamic acid, 
                                Glycine, Histidine, Isoleucine, Leucine, Lysine, Methionine, Phenylalanine, Proline, 
                                Serine, Threonine, Tryptophan, Tyrosine, Valine][required]
        - residue_sequence_number[number][required]
        - residue_additional_information[String][type=text area][optional]

    ## 3. Atom: 
        - atom_id[pattern="id_[0-9]*"][String][required]
        - atom_name[String][required] -> Ex. CA (carboni alfa)
        - atom_x_coordinate[number][optional]
        - atom_y_coordinate[number][optional]
        - atom_z_coordinate[number][optional]
        - atom_temperature_factor[number][optional]
        - atom_element_symbol[String][required] -> Ex. C (carboni)
