/*
  @name = AtomManagement Component Spec TS
  @author = David Medel / Esther Vendrell
  @version = 0.0.1
  @description = Unit tests of the AtomManagement component
  @date = 03-02-2020
*/

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtomManagementComponent } from './atom-management.component';

describe('AtomManagementComponent', () => {
  let component: AtomManagementComponent;
  let fixture: ComponentFixture<AtomManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtomManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtomManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
