/*
  @name = Atom-management Component TS
  @author = David Medel / Esther Vendrell
  @version = 0.0.1
  @description = TypeScript of the Atom-management component
  @date = 03-02-2020
*/

//imports
import { Component, OnInit, HostListener, ViewChild } from '@angular/core';
import { Atom } from '../model/atom/Atom';
import { AtomService } from '../services/atom/atom.service';
import { State } from '../model/atom/State';

//Component, selector tag...
@Component({
  selector: 'app-atom-management',
  templateUrl: './atom-management.component.html',
  styleUrls: ['./atom-management.component.css']
})
export class AtomManagementComponent implements OnInit {

  //asign the atomManagementForm to a variable
  @ViewChild('atomManagementForm', null)
  atomManagementForm: HTMLFormElement;

  //Filter properties
  temperature: number;
  nameFilter: string = "";
  symbolFilter: string = "";

  //Pagination properties
  currentPage: number;
  itemsPerPage: number;

  //properties inicialized
  Atoms: Atom[] = [];
  AtomsFiltered: Atom[] = [];
  arr_state: State[] = [];

  //properties
  AtomSelected: Atom;
  auxAtom: Atom;

  /**
   * Creates an instance of AtomManagementComponent
   * @param atomService 
   * @memberof AtomManagementComponent
   */
  constructor(private atomService: AtomService) { }

  /**
   * @name ngOnInit
   * @description This method is a default method that is called when the page is loading
   * it calls some methods that inicialize and fills the properties/vars
   * and instance the auxAtom and set some of his properties to default values
   * @memberof AtomManagementComponent
   */
  ngOnInit() {
    this.Atoms = this.atomService.generateAtoms();
    this.arr_state = this.atomService.generateState();
    this.AtomsFiltered = this.Atoms;

    this.temperature = 6500;
    this.itemsPerPage = 10;
    this.currentPage = 1;

    this.auxAtom = new Atom();
    this.auxAtom.state = this.arr_state[0];
    this.filter();
  }

  /**
   * @name filter
   * @description This function shows the diferents atoms, depending on the properties that you selected
   * it can filter by state and temperature, by name and by symbol
   * @memberof AtomManagementComponent
   */
  filter() {
    this.AtomsFiltered = this.Atoms.
      filter(Atom => {
        let nameValid: boolean = false;
        let symbolValid: boolean = false;
        let stateTemperatureValid: boolean = false;

        //filter by temperature and state
        if (this.itemsPerPage > 0 && this.itemsPerPage != null) {
          switch (this.auxAtom.state.id) {
            case 0: { //gas
              stateTemperatureValid = (this.temperature > Atom.tChangeFase2 && Atom.tChangeFase1 != null && Atom.tChangeFase2 != null);
              break;
            }
            case 1: { //liquid
              stateTemperatureValid = (this.temperature <= Atom.tChangeFase2 && this.temperature > Atom.tChangeFase1 && Atom.tChangeFase1 != null && Atom.tChangeFase2 != null);
              break;
            }
            case 2: { //solid
              stateTemperatureValid = (this.temperature <= Atom.tChangeFase1 && Atom.tChangeFase1 != null && Atom.tChangeFase2 != null);
              break;
            }
            case 3: { //unknown
              stateTemperatureValid = (Atom.tChangeFase1 == null && Atom.tChangeFase2 == null);
              this.temperature = 6500;
              break;
            }
          }

          // IndexOf: Returns the position of the 
          // first occurrence of a substring.
          // Otherwise returns -1

          //Filter by name
          if (this.nameFilter && this.nameFilter != "") {
            if (Atom.name.toLowerCase().indexOf
              (this.nameFilter.toLowerCase()) != -1) {
              nameValid = true;
            }
          } else {
            nameValid = true;
          }

          //Flter by symbol
          if (this.symbolFilter && this.symbolFilter != "") {
            if (Atom.symbol.toLowerCase().indexOf
              (this.symbolFilter.toLowerCase()) != -1) {
              symbolValid = true;
            }
          } else {
            symbolValid = true;
          }
        } 
        return nameValid && stateTemperatureValid && symbolValid;
      })
  }

  /**
   * @name onClick
   * @description function that get the atm that you selected and set the AtomSelected
   * @param atm 
   * @memberof AtomManagementComponent
   */
  onClick(atm: Atom) {
    this.AtomSelected = atm;
  }

  /**
   * @name removeAtm
   * @description function that removes the atm that you select
   * @param atm 
   * @memberof AtomManagementComponent
   */
  removeAtm(atm: Atom) {
    this.Atoms.splice(this.Atoms.indexOf(atm), 1);
    this.AtomsFiltered.splice(this.AtomsFiltered.indexOf(atm), 1);
  }
}
