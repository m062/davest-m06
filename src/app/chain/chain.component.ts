/*
  @name = Chain Component TS
  @author = David Medel / Esther Vendrell
  @version = 0.0.1
  @description = TypeScript of the Chain component
  @date = 03-02-2020
*/

//imports
import { Component, OnInit, ViewChild } from '@angular/core';
import { Chain } from '../model/chain/Chain';
import { StructureOfChain } from '../model/chain/structureOfChain';
//Chain Service
import { ChainService } from '../services/chain/chain.service';
//cookies
import { CookieService } from 'ngx-cookie-service';

/**
 * @export
 * @class ChainComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-chain',
  templateUrl: './chain.component.html',
  styleUrls: ['./chain.component.css']
})

export class ChainComponent implements OnInit {
  
  @ViewChild('chainForm', null) 
  chainForm: HTMLFormElement;

  //properties
  objChain: Chain;
  chainType: string;
  typeOfChains: StructureOfChain[]=[];
  //cookie object
  cookieObj: any;

  /**
   *Creates an instance of ChainComponent.
   * @param {CookieService} cookieService
   * @param {ChainService} chainService
   * @memberof ChainComponent
   */
  constructor(
    private cookieService: CookieService,
    private chainService: ChainService
  ) { }

  /**
   * @name ngOnInit
   * @description This method is a default method that is called when the page is loading
   * it calls some methods that inicialize and fills the properties,
   * instance the objChain and set his properties to default values
   */
  ngOnInit() {
    //create the type of chain
    this.typeOfChains = this.chainService.createTypeOfChain();
    //create the object chain
    this.objChain = new Chain();
    //initialize type of chains
    this.objChain.$typeOfChain = this.typeOfChains[0];

    this.initializeForm();    
    this.getCookie();
  }

  /**
   * @name initializeForm
   * @description This method is for restart all the different params of the chainForm. 
   */
  initializeForm(){
    this.chainForm.reset();
    this.chainForm.form.markAsPristine();
    this.chainForm.form.markAsUntouched();

    //If this.reservation is null is because
    //no input param has been received.
    //Otherwise, we should be careful not to
    //smash the parameter

    //if this.objChain do not exist
    if(!this.objChain){
      this.objChain = new Chain();

      this.objChain.$chain_sequence_length = null;
      this.objChain.$typeOfChain = this.typeOfChains[0];
    }
    
  }

  /**
   * @name calculateSequenceLength
   * @description function that calculate the length of chain_sequence and save it in chain_sequence_length variable. 
   */
  calculateSequenceLength(){
    this.objChain.$chain_sequence_length = this.objChain.$chain_sequence.length;
  }

  /**
   * @name chain
   * @description Method that shows the objChain and his properties in console
   */
  chain(): void {
    this.cookieService.set("chain", JSON.stringify(this.objChain));
    console.log(this.cookieService.set("chain", JSON.stringify(this.objChain)));
    console.log(this.objChain);
  }

  /**
   * @name getCookie
   * @description Method that gets all the cookies of chain params.
   */
  getCookie() {
    if (this.cookieService.check("chain")) {
      console.log("CookieChain exists")

      this.cookieObj = JSON.parse(this.cookieService.get("chain"));
      
      Object.assign(this.objChain, this.cookieObj);

      this.objChain.$typeOfChain = this.typeOfChains[this.cookieObj.typeOfChain.id];
    }
  }

}
