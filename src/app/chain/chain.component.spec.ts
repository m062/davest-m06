/*
  @name = Chain Component Spec TS
  @author = David Medel / Esther Vendrell
  @version = 0.0.1
  @description = Unit tests of the Chain component
  @date = 03-02-2020
*/

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChainComponent } from './chain.component';

describe('ChainComponent', () => {
  let component: ChainComponent;
  let fixture: ComponentFixture<ChainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
