/*
  @name = Atom Component TS
  @author = David Medel / Esther Vendrell
  @version = 0.0.1
  @description = TypeScript of the Atom component
  @date = 03-02-2020
*/

//imports
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Atom } from '../model/atom/Atom';
import { Occurrence } from '../model/atom/Occurrence';
import { Orbital} from '../model/atom/Orbital';
import { State} from '../model/atom/State';
import {Trend} from '../model/atom/Trend';
import {SubTrend} from '../model/atom/SubTrend';
//atom Service
import { AtomService } from '../services/atom/atom.service';

//Component, selector tag...
@Component({
  selector: 'app-atom',
  templateUrl: './atom.component.html',
  styleUrls: ['./atom.component.css']
})


export class AtomComponent implements OnInit {

  //properties
  @Input() auxAtom: Atom;
  objAtom: Atom;

  //inicialize vars
  arr_occurrence: Occurrence[]=[];
  arr_sfdp: Orbital[]=[];
  arr_state: State[]=[];
  arr_trend: Trend[]=[];
  arr_subtrend_metal: SubTrend[]=[];
  arr_subtrend_nonmetal: SubTrend[]=[];

  /**
   * @type {HTMLFormElement}
   * @memberof AtomComponent
   */
  @ViewChild('orbitalCheckBox',null)
  orbitalCheckBox: HTMLFormElement;


  
  /**
   *Creates an instance of AtomComponent.
   * @param {AtomService} atomService
   * @memberof AtomComponent
   */
  constructor(
    private atomService: AtomService
  ) { }

  /**
   * @name ngOnInit
   * @description This method is a default method that is called when the page is loading
   * it calls some methods that inicialize and fills the properties,
   * and instance the objAtom and set his properties to default values
   */
  ngOnInit() {
    this.arr_occurrence = this.atomService.createOccurrence();
    this.arr_sfdp = this.atomService.createOrbital();
    this.arr_state = this.atomService.createState();
    this.arr_trend = this.atomService.createTrend();
    this.arr_subtrend_metal = this.atomService.createSubTrendMetal();
    this.arr_subtrend_nonmetal = this.atomService.createSubTrendNonMetal();

    this.initializeForm();
  }

  /**
   * @name initializeForm
   * @description This method is for restart all the different params of the objAtom. 
   */
  initializeForm(){
    //If this.objAtom is null is because no input param has been received
    //Otherwise, we should be careful not to smash the parameter
    if(!this.objAtom){
      this.objAtom = new Atom();
    
      this.objAtom.trend = this.arr_trend[0];
      this.objAtom.subtrend = this.arr_subtrend_metal[0];
      this.objAtom.state = this.arr_state[0];
      this.objAtom.occurrence = this.arr_occurrence[0];
      this.objAtom.orbital = [];
    }else{
      this.fillFields();
    }
  }

  /**
   * @name ngAfterViewInit
   * @description This method start after init
   * @memberof AtomComponent
   */
  ngAfterViewInit(){
    this.fillCheckbox();
  }

  /**
   * @name ngOnChanges
   * @description onChange methods of html
   * @memberof AtomComponent
   */
  ngOnChanges(){
    this.fillFields();
    this.fillCheckbox();
  }

  /**
   * @name fillFields
   * @description we inicialize the var atom, and set the values of the auxAtom
   * received from the atom-management, we should be careful with the subtrend, that can be null
   * @memberof AtomComponent
   */
  fillFields(){
    this.objAtom = new Atom();
    this.objAtom.num = this.auxAtom.num;
    this.objAtom.name = this.auxAtom.name;
    this.objAtom.symbol = this.auxAtom.symbol;
    this.objAtom.trend = this.arr_trend[this.auxAtom.trend.id];
    this.objAtom.trend = this.arr_trend[this.auxAtom.trend.id];
     switch(this.auxAtom.trend.id) {
       case 0: {
         this.objAtom.subtrend = this.arr_subtrend_metal[this.auxAtom.subtrend.id];
          break;
       }
       case 2: {
         this.objAtom.subtrend = this.arr_subtrend_nonmetal[this.auxAtom.subtrend.id];
          break;
       }
       default: {
        this.objAtom.subtrend = null;
          break;
       }
     }
     this.objAtom.state = this.arr_state[this.auxAtom.state.id];
     this.objAtom.occurrence = this.arr_occurrence[this.auxAtom.occurrence.id];
     this.objAtom.tChangeFase1 = this.auxAtom.tChangeFase1;
     this.objAtom.tChangeFase2 = this.auxAtom.tChangeFase2;
  }

  /**
   * @name fillCheckbox
   * @description function that inicializes the values of the checkbox to false, and set it to true
   * depending on the values of auxAtom 
   * @memberof AtomComponent
   */
  fillCheckbox(){
    if(this.orbitalCheckBox && this.orbitalCheckBox.nativeElement.children.length != 0){
      this.objAtom.orbital = [];
      for (let i=0; i<4; i++){
        this.orbitalCheckBox.nativeElement.children[i].children[0].checked=false; 
      }
      //We create new Objects SpecialRequests because the
      //Object.assign method doesn't give the full method structure of the class
      if(this.auxAtom){
        for(let orbital of this.auxAtom.orbital){
          //  this.objAtom.orbital.push(new Orbital(orbital.id, orbital.orbital));
           this.orbitalCheckBox.nativeElement.children[orbital.id].children[0].checked=true;       
         }
      }
    }
    
  }

  /**
   *
   * @name atom
   * @description Method that shows the objAtom and his properties in Console
   */
  atom(): void {
    console.log(this.objAtom);
  }

  /**
   *
   * @name changeSubtrend
   * @description Method that fills the objAtom.subtrend with the corresponding subtrend
   * depending on the selected trend in the template
   */
  changeSubtrend(): void{
    switch(this.objAtom.trend.id) {
      case 0: {
        this.objAtom.subtrend = this.arr_subtrend_metal[0];
         break;
      }
      case 2: {
        this.objAtom.subtrend = this.arr_subtrend_nonmetal[0];
         break;
      }
      default: {
        this.objAtom.subtrend = null;
         break;
      }
   }
  }

  /**
   *
   * @name addRemoveOrbital
   * @description Method that add and remove the orbital on objAtom.orbital
   * depending on the checkbox in the template
   */
  addRemoveOrbital(sfdp: Orbital): void{
    //(array).splice(index, num_elem)
    //(array).idexOf(obj) return -1 if not found, return the index if founds
    let myIndex: number = this.objAtom.orbital.indexOf(sfdp);
    if (myIndex == -1){
      this.objAtom.orbital.push(sfdp);
    }else{
      this.objAtom.orbital.splice(myIndex, 1);
    }
  }

}
