/*
  @name = NavbarComponent TS
  @author = David Medel / Esther Vendrell
  @version = 0.0.1
  @description = TypeScript of the NavbarComponent
  @date = 03-02-2020
*/

//imports
import { Component, OnInit } from '@angular/core';

//Component, selector gat...
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  //Default constructor
  constructor() { }

  //Default ngOnInit
  ngOnInit() {
  }

  //inicialize the value of the navbar
  navbarOpen = false;

  /**
   * @name toggleNavbar
   * @description opens the navbar
   */
  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
  }

}
