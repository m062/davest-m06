/*
  @name = PageNotFoundComponent TS
  @author = David Medel / Esther Vendrell
  @version = 0.0.1
  @description = TypeScript of the PageNotFoundComponent
  @date = 03-02-2020
*/

import { Component, OnInit } from '@angular/core';

//Component, selector tag...
@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.css']
})
export class PageNotFoundComponent implements OnInit {

  //default constructor
  constructor() { }

  //default ngOnInit
  ngOnInit() {
  }

}
