/*
  @name = AminoacidService TS
  @author = David Medel / Esther Vendrell
  @version = 0.0.1
  @description = TypeScript of the AminoacidService
  @date = 03-02-2020
*/

//imports
import { Injectable } from '@angular/core';
import { Essentiality} from '../../model/aminoacid/Essentiality';
import { ChainPropertie} from '../../model/aminoacid/ChainPropertie';
import {FullInfoAmino} from '../../model/aminoacid/FullInfoAmino';

//Injectable
@Injectable({
  providedIn: 'root'
})
export class AminoacidService {
  //vars
  arr_aminoacids = [];

  //default constructor
  constructor() { }

  /**
   *
   * @name createEssentiality
   * @description Function that fills the arr_essentiality and returns it
   * @returns essentialities
   */
   createEssentiality(): Essentiality[]{
    let essentialities: Essentiality[] = [];

    let essentialityAux: string[]=["essential","non essential","conditional"];

    for(let i:number=0; i<essentialityAux.length;i++){
      essentialities.push(new Essentiality(i, essentialityAux[i]));
    }

    return essentialities;
  }

  /**
   *
   * @name createChainPropertie
   * @description Function that fills the arr_chainProperties and returns it
   * @returns chainProperties
   */
  createChainPropertie(): ChainPropertie[] {

    let chainProperties: ChainPropertie[] = [];
    let chainPropAux: string[]=["polar","non polar","acidic polar", "basic polar", "Aromatic"];
    
    for(let i:number=0; i<chainPropAux.length;i++){
      chainProperties.push(new ChainPropertie(i, chainPropAux[i]));
    }

    return chainProperties;
  }

  /**
   *
   * @name createTableFullInfoAmino
   * @description Function that fills the arr_aa and returns it
   * @returns arr_aa
   */
  createTableFullInfoAmino(): FullInfoAmino[]{

    let arr_aa: FullInfoAmino[] = [];

    arr_aa.push(new FullInfoAmino("Alanine","Ala","A",new Essentiality(1,"non essential"),new ChainPropertie(1,"non polar")));
    arr_aa.push(new FullInfoAmino("Arginine","Arg","R",new Essentiality(2,"conditional"),new ChainPropertie(3,"basic polar")));
    arr_aa.push(new FullInfoAmino("Asparagine","Asn","N",new Essentiality(1,"non essential"),new ChainPropertie(0,"polar")));
    arr_aa.push(new FullInfoAmino("Aspartic acid","Asp","D",new Essentiality(1,"non essential"),new ChainPropertie(2,"acidic polar")));
    arr_aa.push(new FullInfoAmino("Cysteine","Cys","C",new Essentiality(2,"conditional"),new ChainPropertie(1,"non polar")));
    arr_aa.push(new FullInfoAmino("Glutamic acid","Glu","E",new Essentiality(1,"non essential"),new ChainPropertie(2,"acidic polar")));
    arr_aa.push(new FullInfoAmino("Glutamine","Gln","Q",new Essentiality(2,"conditional"),new ChainPropertie(0,"polar")));
    arr_aa.push(new FullInfoAmino("Glycine","Gly","G",new Essentiality(2,"conditional"),new ChainPropertie(1,"non polar")));
    arr_aa.push(new FullInfoAmino("Histidine","His","H",new Essentiality(0,"essential"),new ChainPropertie(4,"Aromatic")));
    arr_aa.push(new FullInfoAmino("Isoleucine","Ile","I",new Essentiality(0,"essential"),new ChainPropertie(1,"non polar")));
    arr_aa.push(new FullInfoAmino("Leucine","Leu","L",new Essentiality(0,"essential"),new ChainPropertie(1,"non polar")));
    arr_aa.push(new FullInfoAmino("Lysine","Lys","K",new Essentiality(0,"essential"),new ChainPropertie(3,"basic polar")));
    arr_aa.push(new FullInfoAmino("Methionine","Met","M",new Essentiality(0,"essential"),new ChainPropertie(1,"non polar")));
    arr_aa.push(new FullInfoAmino("Phenylalanine","Phe","F",new Essentiality(0,"essential"),new ChainPropertie(4,"Aromatic")));
    arr_aa.push(new FullInfoAmino("Proline","Pro","P",new Essentiality(2,"conditional"),new ChainPropertie(1,"non polar")));
    arr_aa.push(new FullInfoAmino("Serine","Ser","S",new Essentiality(2,"conditional"),new ChainPropertie(0,"polar")));
    arr_aa.push(new FullInfoAmino("Threonine","Thr","T",new Essentiality(0,"essential"),new ChainPropertie(0,"polar")));
    arr_aa.push(new FullInfoAmino("Tryptophan","Trp","W",new Essentiality(0,"essential"),new ChainPropertie(4,"Aromatic")));
    arr_aa.push(new FullInfoAmino("Tyrosine","Tyr","Y",new Essentiality(2,"conditional"),new ChainPropertie(4,"Aromatic")));
    arr_aa.push(new FullInfoAmino("Valine","Val","V",new Essentiality(0,"essential"),new ChainPropertie(1,"non polar")));

    return arr_aa;
  }

  /**
   * @name enter
   * @description Method that set the arr_aminoacids with the triplet of bases, depending on the aa
   * @param aminoacidName 
   * @returns arr_aminoacids
   */
  enter(aminoacidName) {

    switch(aminoacidName) {
      case "Alanine": {
        this.arr_aminoacids.push("GCU");
        this.arr_aminoacids.push("GCC");
        this.arr_aminoacids.push("GCA");
        this.arr_aminoacids.push("GCG");
        break;
      }
      case "Arginine": {
        this.arr_aminoacids.push("CGU");
        this.arr_aminoacids.push("CGC");
        this.arr_aminoacids.push("CGA");
        this.arr_aminoacids.push("CGG");
        this.arr_aminoacids.push("AGA");
        this.arr_aminoacids.push("AGG");
        break;
      }
      case "Asparagine": {
        this.arr_aminoacids.push("AAU");
        this.arr_aminoacids.push("AAC");
        break;
      }
      case "Aspartic acid": {
        this.arr_aminoacids.push("GAU");
        this.arr_aminoacids.push("GAC");
        break;
      }
      case "Cysteine": {
        this.arr_aminoacids.push("UGU");
        this.arr_aminoacids.push("UGC");
        break;
      }
      case "Glutamic acid": {
        this.arr_aminoacids.push("GAA");
        this.arr_aminoacids.push("GAG");
        break;
      }
      case "Glutamine": {
        this.arr_aminoacids.push("CAA");
        this.arr_aminoacids.push("CAG");
        break;
      }
      case "Glycine": {
        this.arr_aminoacids.push("GGU");
        this.arr_aminoacids.push("GGC");
        this.arr_aminoacids.push("GGA");
        this.arr_aminoacids.push("GGG");
        break;
      }
      case "Histidine": {
        this.arr_aminoacids.push("CAU");
        this.arr_aminoacids.push("CAC");
        break;
      }
      case "Isoleucine": {
        this.arr_aminoacids.push("AUU");
        this.arr_aminoacids.push("AUC");
        this.arr_aminoacids.push("AUA");
        break;
      }
      case "Leucine": {
        this.arr_aminoacids.push("UUA");
        this.arr_aminoacids.push("UUG");
        this.arr_aminoacids.push("CUU");
        this.arr_aminoacids.push("CUC");
        this.arr_aminoacids.push("CUA");
        this.arr_aminoacids.push("CUG");
        break;
      }
      case "Lysine": {
        this.arr_aminoacids.push("AAA");
        this.arr_aminoacids.push("AAG");
        break;
      }
      case "Methionine": {
        this.arr_aminoacids.push("AUG");
        break;
      }
      case "Phenylalanine": {
        this.arr_aminoacids.push("UUU");
        this.arr_aminoacids.push("UUC");
        break;
      }
      case "Proline": {
        this.arr_aminoacids.push("CCU");
        this.arr_aminoacids.push("CCC");
        this.arr_aminoacids.push("CCA");
        this.arr_aminoacids.push("CCG");
        break;
      }
      case "Serine": {
        this.arr_aminoacids.push("UCU");
        this.arr_aminoacids.push("UCC");
        this.arr_aminoacids.push("UCA");
        this.arr_aminoacids.push("UCG");
        this.arr_aminoacids.push("AGU");
        this.arr_aminoacids.push("AGC");
        break;
      }
      case "Threonine": {
        this.arr_aminoacids.push("ACU");
        this.arr_aminoacids.push("ACC");
        this.arr_aminoacids.push("ACA");
        this.arr_aminoacids.push("ACG");
        break;
      }
      case "Tryptophan": {
        this.arr_aminoacids.push("UGG");
        break;
      }
      case "Tyrosine": {
        this.arr_aminoacids.push("UAU");
        this.arr_aminoacids.push("UAC");
        break;
      }
      case "Valine": {
        this.arr_aminoacids.push("GUU");
        this.arr_aminoacids.push("GUC");
        this.arr_aminoacids.push("GUA");
        this.arr_aminoacids.push("GUG");
        break;
      }
    }
    return this.arr_aminoacids;
  }

  /**
   * @name leave
   * @description function that set the arr_aminoacids to []
   */
  leave(): void {
    this.arr_aminoacids = [];
  }
}
