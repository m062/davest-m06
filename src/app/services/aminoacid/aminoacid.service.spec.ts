/*
  @name = Aminoacid Service Spec TS
  @author = David Medel / Esther Vendrell
  @version = 0.0.1
  @description = Unit tests of the Aminoacid Service
  @date = 03-02-2020
*/

import { TestBed } from '@angular/core/testing';

import { AminoacidService } from './aminoacid.service';

describe('AminoacidService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AminoacidService = TestBed.get(AminoacidService);
    expect(service).toBeTruthy();
  });
});
