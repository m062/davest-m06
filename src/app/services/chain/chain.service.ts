/*
  @name = ChainService TS
  @author = David Medel / Esther Vendrell
  @version = 0.0.1
  @description = TypeScript of the ChainService
  @date = 03-02-2020
*/

//Imports
import { Injectable } from '@angular/core';
import { StructureOfChain } from 'src/app/model/chain/structureOfChain';

//Injectable
@Injectable({
  providedIn: 'root'
})
export class ChainService {

  //default constructor
  constructor() { }

  /**
   * @name createTypeOfChain
   * @description function that register all types of chains in an array and push it to typeOfChains array (a property)
   * @returns typeOfChains
   */
   createTypeOfChain(): StructureOfChain[] {

    let typeOfChains: StructureOfChain[] = [];
    let typeChainAux: string[]=["Primary", "Secondary", "Tertiary", "Quaternary"];

    for(let i:number=0; i<typeChainAux.length;i++){
      typeOfChains.push(new StructureOfChain
          (i,typeChainAux[i]));
    }

    return typeOfChains;
  }
  
}
