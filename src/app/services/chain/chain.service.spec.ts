/*
  @name = Chain Service Spec TS
  @author = David Medel / Esther Vendrell
  @version = 0.0.1
  @description = Unit tests of the Chain Service
  @date = 03-02-2020
*/

import { TestBed } from '@angular/core/testing';

import { ChainService } from './chain.service';

describe('ChainService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ChainService = TestBed.get(ChainService);
    expect(service).toBeTruthy();
  });
});
