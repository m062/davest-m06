/*
  @name = ProteinService TS
  @author = David Medel / Esther Vendrell
  @version = 0.0.1
  @description = TypeScript of the ProteinService
  @date = 03-02-2020
*/

//imports
import { Injectable } from '@angular/core';
import { ComponentOfProtein } from 'src/app/model/protein/componentOfProtein';
import { TypeOfProtein } from 'src/app/model/protein/typeOfProtein';
import { FunctionModel } from 'src/app/model/protein/functionModel';

//Injectable
@Injectable({
  providedIn: 'root'
})
export class ProteinService {

  constructor() { }

  /**
   * @name createComponentOfProtein
   * @description function that register the component of a protein in an array and push it to componentOfProteins (a property),
   * @returns componentOfProteins
   */
   createComponentOfProtein(): ComponentOfProtein[] {
    
    let componentOfProteins: ComponentOfProtein[] = [];
    
    let componentProteinAux: string[]=["Simple (aminoacids)", "Conjugated", "Derived"];

    for(let i:number=0; i<componentProteinAux.length;i++){
      componentOfProteins.push(new ComponentOfProtein
          (i,componentProteinAux[i]));
    }

    return componentOfProteins;
  }

  /**
   * @name createTypeOfProtein
   * @description function that register the type of protein (animal or vegetable) and push it to typeOfProteins property.
   * @returns typeOfProteins
   */
  createTypeOfProtein(): TypeOfProtein[] {

    let typeOfProteins: TypeOfProtein[] = [];
    let typeProteinAux: string[]=["Animal",
    "Vegetable"];

    for(let i:number=0; i<typeProteinAux.length;i++){
      typeOfProteins.push(new TypeOfProtein
          (i,typeProteinAux[i]));
    }
    return typeOfProteins;
  }

  /**
   * @name createFunctionOfProtein
   * @description function that register all functions of proteins in an array and push it to functionOfProteins array (a property)
   * @returns functionOfProteins
   */
  createFunctionOfProtein(): FunctionModel[] {
    let functionOfProteins: FunctionModel[] = [];
    let functionProtAux: string[] = ["Hormonal",
    "Enzymatic","Structural", "Defensive", "Storage", "Conveyor",
    "Receiver", "Motor"];

    for(let i:number=0; i<functionProtAux.length;i++){
      functionOfProteins.push(new FunctionModel
          (i,functionProtAux[i]));
    }
    return functionOfProteins;
  }
}
