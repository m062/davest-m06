/*
  @name = Protein Service Spec TS
  @author = David Medel / Esther Vendrell
  @version = 0.0.1
  @description = Unit tests of the Protein Service
  @date = 03-02-2020
*/

import { TestBed } from '@angular/core/testing';

import { ProteinService } from './protein.service';

describe('ProteinService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProteinService = TestBed.get(ProteinService);
    expect(service).toBeTruthy();
  });
});
