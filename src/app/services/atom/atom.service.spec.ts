/*
  @name = Atom Service Spec TS
  @author = David Medel / Esther Vendrell
  @version = 0.0.1
  @description = Unit tests of the Atom Service
  @date = 03-02-2020
*/

import { TestBed } from '@angular/core/testing';

import { AtomService } from './atom.service';

describe('AtomService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AtomService = TestBed.get(AtomService);
    expect(service).toBeTruthy();
  });
});
