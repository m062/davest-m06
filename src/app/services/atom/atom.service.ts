/*
  @name = AtomService TS
  @author = David Medel / Esther Vendrell
  @version = 0.0.1
  @description = TypeScript of the AtomService
  @date = 03-02-2020
*/

//imports
import { Injectable } from '@angular/core';
import { Atom } from '../../model/atom/Atom';
import { Occurrence } from '../../model/atom/Occurrence';
import { Orbital} from '../../model/atom/Orbital';
import { State} from '../../model/atom/State';
import {Trend} from '../../model/atom/Trend';
import {SubTrend} from '../../model/atom/SubTrend';

//Injectable
@Injectable({
  providedIn: 'root'
})
export class AtomService {
  
  //default constructor
  constructor() { }

  /**
   * @name generateAtoms
   * @description Funtion that fills the arr_atoms and returns it
   * @returns arr_atoms
   */
  generateAtoms(): Atom[]{

    let arr_atoms: Atom[] = [];

    arr_atoms.push(new Atom(1,'Hydrogen','H', new Occurrence(0,'Primordial'), [new Orbital(0,'s')], new Trend(2,'Nonmetal'), new SubTrend(0,'Reactive nonmetal'), 15, 21, new State(0, 'Gas')));
    arr_atoms.push(new Atom(2,'Helium','He', new Occurrence(0,'Primordial'), [new Orbital(0,'s')], new Trend(2,'Nonmetal'), new SubTrend(1,'Noble gas'), -1, 5, new State(0, 'Gas')));
    arr_atoms.push(new Atom(3,'Lithium','Li', new Occurrence(0,'Primordial'), [new Orbital(0,'s')], new Trend(0,'Metal'), new SubTrend(0,'Alkali metal'), 454, 1615, new State(2, 'Solid'))); 
    arr_atoms.push(new Atom(4,'Beryllium','Be', new Occurrence(0,'Primordial'), [new Orbital(0,'s')], new Trend(0,'Metal'), new SubTrend(1,'Alkali earth metal'), 1560, 2743, new State(2, 'Solid')));
    arr_atoms.push(new Atom(5,'Boron','B', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p')], new Trend(1,'Metalloid'), new SubTrend(null, null), 2348, 4273, new State(2, 'Solid')));
    arr_atoms.push(new Atom(6,'Carbon','C', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p')], new Trend(2,'Nonmetal'), new SubTrend(0,'Reactive nonmetal'), 3823, 4300, new State(2, 'Solid')));
    arr_atoms.push(new Atom(7,'Nitrogen','N', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p')], new Trend(2,'Nonmetal'), new SubTrend(0,'Reactive nonmetal'), 64, 78, new State(0, 'Gas')));
    arr_atoms.push(new Atom(8,'Oxygen','O', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p')], new Trend(2,'Nonmetal'), new SubTrend(0,'Reactive nonmetal'), 55, 91, new State(0, 'Gas')));
    arr_atoms.push(new Atom(9,'Fluorine','F', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p')], new Trend(2,'Nonmetal'), new SubTrend(0,'Reactive nonmetal'), 54, 86, new State(0, 'Gas')));
    arr_atoms.push(new Atom(10,'Neon','Ne', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p')], new Trend(2,'Nonmetal'), new SubTrend(1,'Noble gas'), 25, 28, new State(0, 'Gas')));
    arr_atoms.push(new Atom(11,'Sodium','Na', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p')], new Trend(0,'Metal'), new SubTrend(0,'Alkali metal'), 371, 1156, new State(2, 'Solid')));
    arr_atoms.push(new Atom(12,'Magnesium','Mg', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p')], new Trend(0,'Metal'), new SubTrend(1,'Alkali earth metal'), 923, 1363, new State(2, 'Solid')));
    arr_atoms.push(new Atom(13,'Aluminium','Al', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p')], new Trend(0,'Metal'), new SubTrend(5,'Post-transitional metal'), 934, 2792, new State(2, 'Solid')));
    arr_atoms.push(new Atom(14,'Silicon','Si', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p')], new Trend(1,'Metalloid'), new SubTrend(null, null), 1687, 3173, new State(2, 'Solid')));
    arr_atoms.push(new Atom(15,'Phosphorus','P', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p')], new Trend(2,'Nonmetal'), new SubTrend(0,'Reactive nonmetal'), 54, 86, new State(2, 'Solid')));
    arr_atoms.push(new Atom(16,'Sulfur','S', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p')], new Trend(2,'Nonmetal'), new SubTrend(0,'Reactive nonmetal'), 389, 718, new State(2, 'Solid')));
    arr_atoms.push(new Atom(17,'Chlorine','Cl', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p')], new Trend(2,'Nonmetal'), new SubTrend(0,'Reactive nonmetal'), 172, 240, new State(0, 'Gas')));
    arr_atoms.push(new Atom(18,'Argon','Ar', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p')], new Trend(2,'Nonmetal'), new SubTrend(1,'Noble gas'), 84, 88, new State(0, 'Gas')));
    arr_atoms.push(new Atom(19,'Potassium','K', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p')], new Trend(0,'Metal'), new SubTrend(0,'Alkali metal'), 337, 1032, new State(2, 'Solid')));
    arr_atoms.push(new Atom(20,'Calcium','Ca', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p')], new Trend(0,'Metal'), new SubTrend(1,'Alkali earth metal'), 1115, 1757, new State(2, 'Solid')));
    arr_atoms.push(new Atom(21,'Scandium','Sc', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), 1814, 3101, new State(2, 'Solid')));
    arr_atoms.push(new Atom(22,'Titanium','Ti', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), 1941, 3560, new State(2, 'Solid')));
    arr_atoms.push(new Atom(23,'Vanadium','V', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), 2183, 3680, new State(2, 'Solid')));
    arr_atoms.push(new Atom(24,'Chromium','Cr', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), 2180, 2944, new State(2, 'Solid')));
    arr_atoms.push(new Atom(25,'Manganese','Mn', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), 1519, 2334, new State(2, 'Solid')));
    arr_atoms.push(new Atom(26,'Iron','Fe', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), 1811, 3134, new State(2, 'Solid')));
    arr_atoms.push(new Atom(27,'Cobalt','Co', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), 1768, 3200, new State(2, 'Solid')));
    arr_atoms.push(new Atom(28,'Nickel','Ni', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), 1728, 3186, new State(2, 'Solid')));
    arr_atoms.push(new Atom(29,'Copper','Cu', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), 1358, 3200, new State(2, 'Solid')));
    arr_atoms.push(new Atom(30,'Zinc','Zn', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), 693, 1180, new State(2, 'Solid')));
    arr_atoms.push(new Atom(31,'Gallium','Ga', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(0,'Metal'), new SubTrend(5,'Post-transitional metal'), 303, 2477, new State(2, 'Solid')));
    arr_atoms.push(new Atom(32,'Germanium','Fe', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(1,'Metalloid'), new SubTrend(null, null), 1212, 3093, new State(2, 'Solid')));
    arr_atoms.push(new Atom(33,'Arsenic','As', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')],new Trend(1,'Metalloid'), new SubTrend(null, null), 887, 887, new State(2, 'Solid'))); //not have liquid fase
    arr_atoms.push(new Atom(34,'Selenium','Se', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(2,'Nonmetal'), new SubTrend(0,'Reactive nonmetal'), 494, 958, new State(2, 'Solid')));
    arr_atoms.push(new Atom(35,'Bromine','Br', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(2,'Nonmetal'), new SubTrend(0,'Reactive nonmetal'), 266, 332, new State(1, 'Liquid')));
    arr_atoms.push(new Atom(36,'Krypton','Kr', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(2,'Nonmetal'), new SubTrend(1,'Noble gas'), 116, 120, new State(0, 'Gas')));
    arr_atoms.push(new Atom(37,'Rubidium','Rb', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(2,'Nonmetal'), new SubTrend(0,'Reactive nonmetal'), 313, 961, new State(2, 'Solid')));
    arr_atoms.push(new Atom(38,'Estrontium','Sr', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(0,'Metal'), new SubTrend(1,'Alkali earth metal'), 1050, 1655, new State(2, 'Solid')));
    arr_atoms.push(new Atom(39,'Ytrium','Y', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), 1799, 3609, new State(2, 'Solid')));
    arr_atoms.push(new Atom(40,'Zirconium','Zr', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), 2128, 4644, new State(2, 'Solid')));
    arr_atoms.push(new Atom(41,'Niobium','Nb', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), 2570, 5017, new State(2, 'Solid')));
    arr_atoms.push(new Atom(42,'Molybdenum','Mo', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), 2896, 4912, new State(2, 'Solid')));
    arr_atoms.push(new Atom(43,'Technetium','Tc', new Occurrence(1,'From decay'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), 2430, 4538, new State(2, 'Solid')));
    arr_atoms.push(new Atom(44,'Ruthenium','Ru', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), 2607, 4423, new State(2, 'Solid')));
    arr_atoms.push(new Atom(45,'Rhodium','Rh', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), 2237, 3968, new State(2, 'Solid')));
    arr_atoms.push(new Atom(46,'Palladium','Pd', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), 1829, 3236, new State(2, 'Solid')));
    arr_atoms.push(new Atom(47,'Silver','Ag', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), 689, 2435, new State(2, 'Solid')));
    arr_atoms.push(new Atom(48,'Cadmium','Cd', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), 595, 1041, new State(2, 'Solid')));
    arr_atoms.push(new Atom(49,'Indium','In', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(0,'Metal'), new SubTrend(5,'Post-transitional metal'), 430, 2345, new State(2, 'Solid')));
    arr_atoms.push(new Atom(50,'Tin','Sn', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(0,'Metal'), new SubTrend(5,'Post-transitional metal'), 505, 2875, new State(2, 'Solid')));
    arr_atoms.push(new Atom(51,'Antimony','Sb', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(1,'Metalloid'), new SubTrend(null, null), 904, 1860, new State(2, 'Solid')));
    arr_atoms.push(new Atom(52,'Tellurium','Te', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(1,'Metalloid'), new SubTrend(null, null), 723, 1261, new State(2, 'Solid')));
    arr_atoms.push(new Atom(53,'Iodine','I', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(2,'Nonmetal'), new SubTrend(0,'Reactive nonmetal'), 387, 457, new State(2, 'Solid')));
    arr_atoms.push(new Atom(54,'Xenon','Xe', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(2,'Nonmetal'), new SubTrend(1,'Noble gas'), 161, 165, new State(0, 'Gas')));
    arr_atoms.push(new Atom(55,'Caesium','Cs', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(2,'Nonmetal'), new SubTrend(0,'Reactive nonmetal'), 302, 944, new State(2, 'Solid')));
    arr_atoms.push(new Atom(56,'Barium','Ba', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(0,'Metal'), new SubTrend(1,'Alkali earth metal'), 1000, 2118, new State(2, 'Solid')));
    arr_atoms.push(new Atom(57,'Lanthanum','La', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d')], new Trend(0,'Metal'), new SubTrend(2,'Lanthonide'), 1193, 3730, new State(2, 'Solid')));
    arr_atoms.push(new Atom(58,'Cerium','Ce', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(2,'Lanthonide'), 1071, 3699, new State(2, 'Solid')));
    arr_atoms.push(new Atom(59,'Praseodymium','Pr', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(2,'Lanthonide'), 1204, 3793, new State(2, 'Solid')));
    arr_atoms.push(new Atom(60,'Neodymium','Nd', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(2,'Lanthonide'), 1297, 3373, new State(2, 'Solid')));
    arr_atoms.push(new Atom(61,'Promethium','Pm', new Occurrence(1,'From decay'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(2,'Lanthonide'), 1373, 3273, new State(2, 'Solid')));
    arr_atoms.push(new Atom(62,'Samarium','Sm', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(2,'Lanthonide'), 1345, 2076, new State(2, 'Solid')));
    arr_atoms.push(new Atom(63,'Europium','Eu', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(2,'Lanthonide'), 1099, 1800, new State(2, 'Solid')));
    arr_atoms.push(new Atom(64,'Gadolinium','Gd', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(2,'Lanthonide'), 1585, 3523, new State(2, 'Solid')));
    arr_atoms.push(new Atom(65,'Terbium','Tb', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(2,'Lanthonide'), 1629, 3503, new State(2, 'Solid')));
    arr_atoms.push(new Atom(66,'Dysprosium','Dy', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(2,'Lanthonide'), 1680, 2840, new State(0, 'Gas')));
    arr_atoms.push(new Atom(67,'Holmium','Ho', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(2,'Lanthonide'), 1734, 2873, new State(2, 'Solid')));
    arr_atoms.push(new Atom(68,'Erbium','Er', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(2,'Lanthonide'), 1795, 3136, new State(2, 'Solid')));
    arr_atoms.push(new Atom(69,'Thulium','Tm', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(2,'Lanthonide'), 1818, 2220, new State(2, 'Solid')));
    arr_atoms.push(new Atom(70,'Ytterbium','Yb', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(2,'Lanthonide'), 1097, 1467, new State(2, 'Solid')));
    arr_atoms.push(new Atom(71,'Lutetium','Lu', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(2,'Lanthonide'), 1925, 3675, new State(2, 'Solid')));
    arr_atoms.push(new Atom(72,'Hafnium','Hf', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), 2506, 4876, new State(2, 'Solid')));
    arr_atoms.push(new Atom(73,'Tantalum','Ta', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), 3290, 5731, new State(2, 'Solid')));
    arr_atoms.push(new Atom(74,'Tungten','W', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), 3695, 6203, new State(2, 'Solid')));
    arr_atoms.push(new Atom(75,'Rhenium','Re', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), 3186, 5596, new State(2, 'Solid')));
    arr_atoms.push(new Atom(76,'Osmium','Os', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), 3306, 5285, new State(2, 'Solid')));
    arr_atoms.push(new Atom(77,'Iridium','Ir', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), 2739, 4701, new State(2, 'Solid')));
    arr_atoms.push(new Atom(78,'Platinum','Pt', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), 1769, 4098, new State(2, 'Solid')));
    arr_atoms.push(new Atom(79,'Gold','Au', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), 1337, 3129, new State(2, 'Solid')));
    arr_atoms.push(new Atom(80,'Mercury','Hg', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), 235, 630, new State(1, 'Liquid')));
    arr_atoms.push(new Atom(81,'Thallium','Ti', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(5,'Post-transitional metal'), 577, 1746, new State(2, 'Solid')));
    arr_atoms.push(new Atom(82,'Lead','Pb', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(5,'Post-transitional metal'), 601, 2022, new State(2, 'Solid')));
    arr_atoms.push(new Atom(83,'Bismuth','Bi', new Occurrence(0,'Primordial'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(5,'Post-transitional metal'), 544, 1837, new State(2, 'Solid')));
    arr_atoms.push(new Atom(84,'Polonium','Po', new Occurrence(1,'From decay'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(5,'Post-transitional metal'), 527, 1235, new State(2, 'Solid')));
    arr_atoms.push(new Atom(85,'Astatine','At', new Occurrence(1,'From decay'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(1,'Metalloid'), new SubTrend(null, null), 575, 610, new State(2, 'Solid')));
    arr_atoms.push(new Atom(86,'Radon','Rn', new Occurrence(1,'From decay'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(2,'Nonmetal'), new SubTrend(1,'Noble gas'), 202, 211, new State(2, 'Solid')));
    arr_atoms.push(new Atom(87,'Francium','Fr', new Occurrence(1,'From decay'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(2,'Nonmetal'), new SubTrend(0,'Reactive nonmetal'), 300, 950, new State(2, 'Solid')));
    arr_atoms.push(new Atom(88,'Radium','Ra', new Occurrence(1,'From decay'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(1,'Alkali earth metal'), 973, 2010, new State(2, 'Solid')));
    arr_atoms.push(new Atom(89,'Actinium','Ac', new Occurrence(1,'From decay'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(3,'Actinide '), 1323, 3471, new State(2, 'Solid')));
    arr_atoms.push(new Atom(90,'Thorium','Th', new Occurrence(1,'From decay'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(3,'Actinide '), 2028, 5061, new State(2, 'Solid')));
    arr_atoms.push(new Atom(91,'Protactinium','Pa', new Occurrence(1,'From decay'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(3,'Actinide '), 2113, 4300, new State(2, 'Solid')));
    arr_atoms.push(new Atom(92,'Uranium','U', new Occurrence(1,'From decay'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(3,'Actinide '), 1900, 2334, new State(2, 'Solid')));
    arr_atoms.push(new Atom(93,'Neptunium','Np', new Occurrence(1,'From decay'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(3,'Actinide '), 910, 4273, new State(2, 'Solid')));
    arr_atoms.push(new Atom(94,'Plutonium','Pu', new Occurrence(1,'From decay'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(3,'Actinide '), 912, 3505, new State(2, 'Solid')));
    arr_atoms.push(new Atom(95,'Americium','Am', new Occurrence(2,'Synthetic'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(3,'Actinide '), 1449, 2880, new State(2, 'Solid')));
    arr_atoms.push(new Atom(96,'Curium','Cm', new Occurrence(2,'Synthetic'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(3,'Actinide '), 1613, 3383, new State(2, 'Solid')));
    arr_atoms.push(new Atom(97,'Berkelium','Bk', new Occurrence(2,'Synthetic'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(3,'Actinide '), null, null, new State(3, 'Unknown')));
    arr_atoms.push(new Atom(98,'Californium','Cf', new Occurrence(2,'Synthetic'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(3,'Actinide '), null, null, new State(3, 'Unknown')));
    arr_atoms.push(new Atom(99,'Einsteinium','Es', new Occurrence(2,'Synthetic'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(3,'Actinide '), null, null, new State(3, 'Unknown')));
    arr_atoms.push(new Atom(100,'Fermium','Fm', new Occurrence(2,'Synthetic'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(3,'Actinide '), null, null, new State(3, 'Unknown')));
    arr_atoms.push(new Atom(101,'Mendelevium','Md', new Occurrence(2,'Synthetic'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(3,'Actinide '), null, null, new State(3, 'Unknown')));
    arr_atoms.push(new Atom(102,'Nobelium','No', new Occurrence(2,'Synthetic'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(3,'Actinide '), null, null, new State(3, 'Unknown')));
    arr_atoms.push(new Atom(103,'Lawrencium','Lr', new Occurrence(2,'Synthetic'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(3,'Actinide '), null, null, new State(3, 'Unknown')));
    arr_atoms.push(new Atom(104,'Rutherfordium','Rf', new Occurrence(2,'Synthetic'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), null, null, new State(3, 'Unknown')));
    arr_atoms.push(new Atom(105,'Dubnium','Db', new Occurrence(2,'Synthetic'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), null, null, new State(3, 'Unknown')));
    arr_atoms.push(new Atom(106,'Seaborgium','Sg', new Occurrence(2,'Synthetic'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), null, null, new State(3, 'Unknown')));
    arr_atoms.push(new Atom(107,'Bohrium','Bh', new Occurrence(2,'Synthetic'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), null, null, new State(3, 'Unknown')));
    arr_atoms.push(new Atom(108,'Hassium','Hs', new Occurrence(2,'Synthetic'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), null, null, new State(3, 'Unknown')));
    arr_atoms.push(new Atom(109,'Meitnerium','Mt', new Occurrence(2,'Synthetic'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(3,'Unknown chemical properties'), new SubTrend(null, null), null, null, new State(3, 'Unknown')));
    arr_atoms.push(new Atom(110,'Darmstadtium','Ds', new Occurrence(2,'Synthetic'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')],new Trend(3,'Unknown chemical properties'), new SubTrend(null, null), null, null, new State(3, 'Unknown')));
    arr_atoms.push(new Atom(111,'Roentgenium','Rg', new Occurrence(2,'Synthetic'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(3,'Unknown chemical properties'), new SubTrend(null, null), null, null, new State(3, 'Unknown')));
    arr_atoms.push(new Atom(112,'Copernicium','Cn', new Occurrence(2,'Synthetic'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')],new Trend(0,'Metal'), new SubTrend(4,'Transition metal'), null, null, new State(3, 'Unknown')));
    arr_atoms.push(new Atom(113,'Nihonium','Nh', new Occurrence(2,'Synthetic'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(3,'Unknown chemical properties'), new SubTrend(null, null), null, null, new State(3, 'Unknown')));
    arr_atoms.push(new Atom(114,'Flerovium','Fl', new Occurrence(2,'Synthetic'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(0,'Metal'), new SubTrend(5,'Post-transitional metal'), null, null, new State(3, 'Unknown')));
    arr_atoms.push(new Atom(115,'Moscovium','Mc', new Occurrence(2,'Synthetic'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(3,'Unknown chemical properties'), new SubTrend(null, null), null, null, new State(3, 'Unknown')));
    arr_atoms.push(new Atom(116,'Livermorium','Lv', new Occurrence(2,'Synthetic'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(3,'Unknown chemical properties'), new SubTrend(null, null), null, null, new State(3, 'Unknown')));
    arr_atoms.push(new Atom(117,'Tennessine','Ts', new Occurrence(2,'Synthetic'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(3,'Unknown chemical properties'), new SubTrend(null, null), null, null, new State(3, 'Unknown')));
    arr_atoms.push(new Atom(118,'Oganesson','Og', new Occurrence(2,'Synthetic'), [new Orbital(0,'s'), new Orbital(1,'p'), new Orbital(2,'d'), new Orbital(3,'f')], new Trend(3,'Unknown chemical properties'), new SubTrend(null, null), null, null, new State(3, 'Unknown')));

    return arr_atoms;
  };

  /**
   * @name generateState
   * @description function that generate the arr_state and returns it
   * @returns arr_state
   */
  generateState(): State[]{
    
    let arr_state = [];
    
    let state: string[]=["Gas","Liquid","Solid","Unknown"];
    
    for(let i:number=0; i<state.length;i++){
      arr_state.push(new State(i, state[i]));
    }

    return arr_state;
  }

  /**
   *
   * @name createOccurrence
   * @description Function that fills the arr_occurrence and returns it
   * @returns occurrences
   */
  createOccurrence(): Occurrence[] {
    let occurrences: Occurrence[] = [];

    let occurrenceAux: string[]=["Primordial","From decay","Synthetic"];
    
    for(let i:number=0; i<occurrenceAux.length;i++){
      occurrences.push(new Occurrence(i, occurrenceAux[i]));
    }

    return occurrences;
  }

  /**
   *
   * @name createOrbital
   * @description Function that fills the arr_sfdp and returns it
   * @returns orbitals
   */
  createOrbital(): Orbital[] {
    let orbitals: Orbital[] = [];

    let sfdp: string[]=["s","f","d","p"];

    for(let i:number=0; i<sfdp.length;i++){
      orbitals.push(new Orbital(i, sfdp[i]));
    }
    return orbitals;
  }

  /**
   *
   * @name createState
   * @description Function that fills the arr_state and returns it
   * @returns states
   */
  createState(): State[] {
    let states: State[] = [];

    let state: string[]=["Gas","Liquid","Solid","Unknown"];
    
    for(let i:number=0; i<state.length;i++){
      states.push(new State(i, state[i]));
    }

    return states;
  }

  /**
   *
   * @name createTrend
   * @description Function that fills the arr_trend and returns it
   * @returns trends
   */
  createTrend(): Trend[] {
    let trends: Trend[] = [];

    let trend: string[]=["Metal","Metalloid","Nonmetal","Unknown chemical properties"];
    
    for(let i:number=0; i<trend.length;i++){
      trends.push(new Trend(i, trend[i]));
    }

    return trends;
  }

  /**
   *
   * @name createSubTrendMetal
   * @description Function that fills the arr_subtrend_metal and returns it
   * @returns subtrendsMetal
   */
  createSubTrendMetal(): SubTrend[] {
    let subtrendsMetal: SubTrend[] = [];

    let subtrend: string[]=["Alkali metal","Alkali earth metal","Lanthonide","Actinide","Transition metal","Post-transitional metal"];
    
    for(let i:number=0; i<subtrend.length;i++){
      subtrendsMetal.push(new SubTrend(i, subtrend[i]));
    }

    return subtrendsMetal;
  }

  /**
   *
   * @name createSubTrendNonMetal
   * @description Function that fills the arr_subtrend_nonmetal and returns it
   * @returns subtrendsNonMetal
   */
  createSubTrendNonMetal(): SubTrend[] {
    let subtrendsNonMetal: SubTrend[] = [];

    let subtrend: string[]=["Reactive nonmetal", "Noble gas"];
    
    for(let i:number=0; i<subtrend.length;i++){
      subtrendsNonMetal.push(new SubTrend(i, subtrend[i]));
    }
    return subtrendsNonMetal;
  }

}
