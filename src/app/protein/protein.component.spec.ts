/*
  @name = Protein Component Spec TS
  @author = David Medel / Esther Vendrell
  @version = 0.0.1
  @description = Unit tests of the Protein component
  @date = 03-02-2020
*/

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProteinComponent } from './protein.component';

describe('ProteinComponent', () => {
  let component: ProteinComponent;
  let fixture: ComponentFixture<ProteinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProteinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProteinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
