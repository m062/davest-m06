/*
  @name = Protein Component TS
  @author = David Medel / Esther Vendrell
  @version = 0.0.1
  @description = TypeScript of the Protein component
  @date = 03-02-2020
*/

//imports
import { Component, OnInit, ViewChild } from '@angular/core';
import { Protein } from '../model/protein/Protein';
import { ComponentOfProtein } from '../model/protein/componentOfProtein';
import { TypeOfProtein } from '../model/protein/typeOfProtein';
import { FunctionModel } from '../model/protein/functionModel';
//Protein Service
import { ProteinService } from '../services/protein/protein.service';
//cookies
import { CookieService } from 'ngx-cookie-service';

/**
 * @export
 * @class ProteinComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-protein',
  templateUrl: './protein.component.html',
  styleUrls: ['./protein.component.css']
})

export class ProteinComponent implements OnInit {

  //asign the proteinForm of html to a variable
  @ViewChild('proteinForm', null) 
  proteinForm: HTMLFormElement;

  //asign the dicCheckBox of html to a variable
  @ViewChild('divCheckBox',null)
  divCheckBox: HTMLFormElement;
  
  //properties
  objProtein: Protein;
  proteinType: string;
  
  componentOfProteins: ComponentOfProtein[]=[];
  typeOfProteins: TypeOfProtein[]=[];
  functionOfProteins: FunctionModel[]=[];
  //cookie object
  cookieObj: any;

  
  /**
   *Creates an instance of ProteinComponent.
   * @param {CookieService} cookieService
   * @param {ProteinService} proteinService
   * @memberof ProteinComponent
   */
  constructor(
    private cookieService: CookieService,
    private proteinService: ProteinService
    ) { }

  /**
   * @name ngOnInit
   * @description This method is a default method that is called when the page is loading
   * it calls some methods that inicialize and fills the properties,
   * instance the objProtein and set his properties to default values
   */
  ngOnInit() {
    this.componentOfProteins = this.proteinService.createComponentOfProtein();
    this.typeOfProteins = this.proteinService.createTypeOfProtein();
    this.functionOfProteins = this.proteinService.createFunctionOfProtein();

    this.initializeForm();    
    this.getCookie();
  }

  /**
   * @name ngAfterViewInit
   * @description This method start after init
   * @memberof ProteinComponent
   */
  ngAfterViewInit(){
    this.getCookieCheckBox();
  }

  /**
   * @name initializeForm
   * @description This method is for restart all the different params of the proteinForm. 
   */
  initializeForm(){
    this.proteinForm.reset();
    this.proteinForm.form.markAsPristine();
    this.proteinForm.form.markAsUntouched();

    //If this.reservation is null is because
    //no input param has been received.
    //Otherwise, we should be careful not to
    //smash the parameter

    //if this.objProtein do not exist
    if(!this.objProtein){
      this.objProtein = new Protein();

      this.objProtein.$componentOfProtein = this.componentOfProteins[0];
      this.objProtein.$typeOfProtein = this.typeOfProteins[0];
      this.objProtein.$functionOfProtein=[];
    }
    
  }

  /**
   * @name getCookieCheckBox
   * @description Method that gets the cookies of protein checkbox. 
   */
  getCookieCheckBox(){
    if(this.cookieObj){
      this.objProtein.$functionOfProtein = [];
      for(let spAux of this.cookieObj.functionOfProtein){
        this.objProtein.$functionOfProtein.push(
          new FunctionModel(spAux.id, spAux.function));
          this.divCheckBox.nativeElement.
          children[spAux.id].children[0].checked=true;
      }
    }
  }

  /**
   * @name protein
   * @description Method that shows the objChain and his properties in console
   */
  protein(): void {
    this.cookieService.set("protein", JSON.stringify(this.objProtein));
    console.log(this.objProtein);
  }

    /**
   * @name getCookie
   * @description Method that gets all the cookies of protein params except the checkbox.
   */
  getCookie() {
    if (this.cookieService.check("protein")) {
      console.log("CookieProtein exists");

      this.cookieObj = JSON.parse(this.cookieService.get("protein"));
      
      Object.assign(this.objProtein, this.cookieObj);
  
      this.objProtein.$componentOfProtein = this.componentOfProteins[this.cookieObj.componentOfProtein.id];
      this.objProtein.$typeOfProtein = this.typeOfProteins[this.cookieObj.typeOfProtein.id];
    }
  }

}
