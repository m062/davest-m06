/*
  @name = Chain Directive TS
  @author = David Medel / Esther Vendrell
  @version = 0.0.1
  @description = TypeScript of the Chain Directive
  @date = 03-02-2020
*/
import { Directive } from '@angular/core';
import { NG_VALIDATORS, AbstractControl } from '@angular/forms';

//Directive, selector tag...
@Directive({
  selector: '[chainPattern]',
  providers: [{provide: NG_VALIDATORS, useExisting: ChainDirectiveDirective, multi: true}]
})

export class ChainDirectiveDirective {

  /**
   * Default constructor
   * @memberof ChainDirectiveDirective
   */
  constructor() { }

  /**
   * @param {AbstractControl} formFieldValidate
   * @returns {{[key:string]: any}}
   * @memberof ChainDirectiveDirective
   */
  //the pattern must be ARNDCQEGHILKMFPSTWYVarndcqeghilkmfpstwyv"
  //pattern="[ARNDCQEGHILKMFPSTWYVarndcqeghilkmfpstwyv]*"
  validate(formFieldValidate: AbstractControl): {[key:string]: any} {
    let validInput: boolean = false;
    
    if (formFieldValidate && formFieldValidate.value ) {
      let reg = new RegExp("^[ARNDCQEGHILKMFPSTWYVarndcqeghilkmfpstwyv]*$");
      validInput = reg.test(formFieldValidate.value);;
    }
    return validInput? null: {'isNotCorrect':true};
  }

}
