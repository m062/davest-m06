/*
  @name = Chain Directive UnitTest
  @author = David Medel / Esther Vendrell
  @version = 0.0.1
  @description = Unit test of the Chain Directive
  @date = 03-02-2020
*/
import { ChainDirectiveDirective } from './chain-directive.directive';

describe('ChainDirectiveDirective', () => {
  it('should create an instance', () => {
    const directive = new ChainDirectiveDirective();
    expect(directive).toBeTruthy();
  });
});
