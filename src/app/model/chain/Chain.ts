/*
  @name = Chain TS
  @author = David Medel / Esther Vendrell
  @version = 0.0.1
  @description = TypeScript of the Chain
  @date = 03-02-2020
*/

//imports
import { StructureOfChain } from './structureOfChain';

export class Chain {
    //properties
    private chain_name: String;
    private chain_sequence: String;
    private chain_sequence_length: number;
    private typeOfChain: StructureOfChain;

	/**
     *Creates an instance of Chain.
     * @param {String} [$chain_name]
     * @param {String} [$chain_sequence]
     * @param {number} [$chain_sequence_length]
     * @param {StructureOfChain} [$typeOfChain]
     * @memberof Chain
     */
    constructor($chain_name?: String, $chain_sequence?: String, $chain_sequence_length?: number, $typeOfChain?: StructureOfChain) {
		this.chain_name = $chain_name;
		this.chain_sequence = $chain_sequence;
		this.chain_sequence_length = $chain_sequence_length;
		this.typeOfChain = $typeOfChain;
	}

    /**
     * Getter $chain_name
     * @return {String}
     */
	public get $chain_name(): String {
		return this.chain_name;
	}

    /**
     * Getter $chain_sequence
     * @return {String}
     */
	public get $chain_sequence(): String {
		return this.chain_sequence;
	}

    /**
     * Getter $chain_sequence_length
     * @return {number}
     */
	public get $chain_sequence_length(): number {
		return this.chain_sequence_length;
	}

    /**
     * Getter $typeOfChain
     * @return {TypeModel}
     */
	public get $typeOfChain(): StructureOfChain {
		return this.typeOfChain;
	}

    /**
     * Setter $chain_name
     * @param {String} value
     */
	public set $chain_name(value: String) {
		this.chain_name = value;
	}

    /**
     * Setter $chain_sequence
     * @param {String} value
     */
	public set $chain_sequence(value: String) {
		this.chain_sequence = value;
	}

    /**
     * Setter $chain_sequence_length
     * @param {number} value
     */
	public set $chain_sequence_length(value: number) {
		this.chain_sequence_length = value;
	}

    /**
     * Setter $typeOfChain
     * @param {TypeModel} value
     */
	public set $typeOfChain(value: StructureOfChain) {
		this.typeOfChain = value;
	}

}