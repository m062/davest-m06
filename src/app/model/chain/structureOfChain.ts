/*
  @name = StructureOfChain TS
  @author = David Medel / Esther Vendrell
  @version = 0.0.1
  @description = TypeScript of the StructureOfChain
  @date = 03-02-2020
*/

export class StructureOfChain {
    //properties
    private id: number;
    private type: string;

	/**
     *Creates an instance of StructureOfChain.
     * @param {number} $id
     * @param {string} $type
     * @memberof StructureOfChain
     */
    constructor($id: number, $type: string) {
		this.id = $id;
		this.type = $type;
	}

    /**
     * Getter $id
     * @return {number}
     */
	public get $id(): number {
		return this.id;
	}

    /**
     * Getter $type
     * @return {string}
     */
	public get $type(): string {
		return this.type;
	}

    /**
     * Setter $id
     * @param {number} value
     */
	public set $id(value: number) {
		this.id = value;
	}

    /**
     * Setter $type
     * @param {string} value
     */
	public set $type(value: string) {
		this.type = value;
	}

}