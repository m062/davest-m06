/*
  @name = ChainProperti TS
  @author = David Medel / Esther Vendrell
  @version = 0.0.1
  @description = TypeScript of the ChainProperti
  @date = 03-02-2020
*/
export class ChainPropertie {
    //properties
    private _id: number;
    private _propertie: string;


	/**
     *Creates an instance of ChainPropertie.
     * @param {number} id
     * @param {string} propertie
     * @memberof ChainPropertie
     */
    constructor(id: number, propertie: string) {
		this._id = id;
		this._propertie = propertie;
	}

    /**
     * Getter id
     * @return {number}
     */
	public get id(): number {
		return this._id;
	}

    /**
     * Getter propertie
     * @return {string}
     */
	public get propertie(): string {
		return this._propertie;
	}

    /**
     * Setter id
     * @param {number} value
     */
	public set id(value: number) {
		this._id = value;
	}

    /**
     * Setter propertie
     * @param {string} value
     */
	public set propertie(value: string) {
		this._propertie = value;
	}

}