/*
  @name = FullInfoAmino TS
  @author = David Medel / Esther Vendrell
  @version = 0.0.1
  @description = TypeScript of the FullInfoAmino
  @date = 03-02-2020
*/

//imports
import { Essentiality } from './Essentiality';
import { ChainPropertie } from './ChainPropertie';

export class FullInfoAmino {
    //properties
    private _fullName: String;
    private _name3letters: String;
    private _name1letter: String;
    private _essenciality: Essentiality;
    private _chainPropertie: ChainPropertie;


	/**
     *Creates an instance of FullInfoAmino.
     * @param {String} fullName
     * @param {String} name3letters
     * @param {String} name1letter
     * @param {Essentiality} essenciality
     * @param {ChainPropertie} chainPropertie
     * @memberof FullInfoAmino
     */
    constructor(fullName?: String, name3letters?: String, name1letter?: String, essenciality?: Essentiality, chainPropertie?: ChainPropertie) {
		this._fullName = fullName;
		this._name3letters = name3letters;
		this._name1letter = name1letter;
		this._essenciality = essenciality;
		this._chainPropertie = chainPropertie;
	}

    /**
     * Getter fullName
     * @return {String}
     */
	public get fullName(): String {
		return this._fullName;
	}

    /**
     * Getter name3letters
     * @return {String}
     */
	public get name3letters(): String {
		return this._name3letters;
	}

    /**
     * Getter name1letter
     * @return {String}
     */
	public get name1letter(): String {
		return this._name1letter;
	}

    /**
     * Getter essenciality
     * @return {Essentiality}
     */
	public get essenciality(): Essentiality {
		return this._essenciality;
	}

    /**
     * Getter chainPropertie
     * @return {ChainPropertie}
     */
	public get chainPropertie(): ChainPropertie {
		return this._chainPropertie;
	}

    /**
     * Setter fullName
     * @param {String} value
     */
	public set fullName(value: String) {
		this._fullName = value;
	}

    /**
     * Setter name3letters
     * @param {String} value
     */
	public set name3letters(value: String) {
		this._name3letters = value;
	}

    /**
     * Setter name1letter
     * @param {String} value
     */
	public set name1letter(value: String) {
		this._name1letter = value;
	}

    /**
     * Setter essenciality
     * @param {Essentiality} value
     */
	public set essenciality(value: Essentiality) {
		this._essenciality = value;
	}

    /**
     * Setter chainPropertie
     * @param {ChainPropertie} value
     */
	public set chainPropertie(value: ChainPropertie) {
		this._chainPropertie = value;
	}

}