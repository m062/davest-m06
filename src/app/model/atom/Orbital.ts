/*
  @name = Orbital TS
  @author = David Medel / Esther Vendrell
  @version = 0.0.1
  @description = TypeScript of the Orbital
  @date = 03-02-2020
*/

export class Orbital {
    //properties
    private _id: number;
    private _orbital: string;


	/**
     *Creates an instance of Orbital.
     * @param {number} id
     * @param {string} orbital
     * @memberof Orbital
     */
    constructor(id: number, orbital: string) {
		this._id = id;
		this._orbital = orbital;
	}

    /**
     * Getter id
     * @return {number}
     */
	public get id(): number {
		return this._id;
	}

    /**
     * Getter orbital
     * @return {string}
     */
	public get orbital(): string {
		return this._orbital;
	}

    /**
     * Setter id
     * @param {number} value
     */
	public set id(value: number) {
		this._id = value;
	}

    /**
     * Setter orbital
     * @param {string} value
     */
	public set orbital(value: string) {
		this._orbital = value;
	}

}