/*
  @name = State TS
  @author = David Medel / Esther Vendrell
  @version = 0.0.1
  @description = TypeScript of the State
  @date = 03-02-2020
*/

export class State {
    //properties
    private _id: number;
    private _state: string;


	/**
     *Creates an instance of State.
     * @param {number} id
     * @param {string} state
     * @memberof State
     */
    constructor(id: number, state: string) {
		this._id = id;
		this._state = state;
	}

    /**
     * Getter id
     * @return {number}
     */
	public get id(): number {
		return this._id;
	}

    /**
     * Getter state
     * @return {string}
     */
	public get state(): string {
		return this._state;
	}

    /**
     * Setter id
     * @param {number} value
     */
	public set id(value: number) {
		this._id = value;
	}

    /**
     * Setter state
     * @param {string} value
     */
	public set state(value: string) {
		this._state = value;
	}

}