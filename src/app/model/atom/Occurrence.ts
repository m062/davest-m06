/*
  @name = Occurrence TS
  @author = David Medel / Esther Vendrell
  @version = 0.0.1
  @description = TypeScript of the Occurrence
  @date = 03-02-2020
*/

export class Occurrence {
    //properties
    private _id: number;
    private _type: string;

	/**
     *Creates an instance of Occurrence.
     * @param {number} id
     * @param {string} type
     * @memberof Occurrence
     */
    constructor(id: number, type: string) {
		this._id = id;
		this._type = type;
    }

    /**
     * Getter id
     * @return {number}
     */
	public get id(): number {
		return this._id;
	}

    /**
     * Getter type
     * @return {string}
     */
	public get type(): string {
		return this._type;
	}

    /**
     * Setter id
     * @param {number} value
     */
	public set id(value: number) {
		this._id = value;
	}

    /**
     * Setter type
     * @param {string} value
     */
	public set type(value: string) {
		this._type = value;
	}


}