/*
  @name = Atom TS
  @author = David Medel / Esther Vendrell
  @version = 0.0.1
  @description = TypeScript of the Atom
  @date = 03-02-2020
*/

//imports
import { Occurrence } from './Occurrence';
import { Orbital } from './Orbital';
import { State } from './State';
import { Trend } from './Trend';
import { SubTrend } from './SubTrend';

export class Atom {
    //properties
    private _num: Number;
    private _name: String;
    private _symbol: String;
    private _occurrence: Occurrence;
    private _orbital: Orbital[];
    private _trend: Trend;
    private _subtrend: SubTrend;
    private _tChangeFase1: Number;
    private _tChangeFase2: Number;
    private _state: State;


	/**
     *Creates an instance of Atom.
     * @param {Number} num
     * @param {String} name
     * @param {String} symbol
     * @param {Occurrence} occurrence
     * @param {Orbital[]} orbital
     * @param {Trend} trend
     * @param {SubTrend} subtrend
     * @param {Number} tChangeFase1
     * @param {Number} tChangeFase2
     * @param {State} state
     * @memberof Atom
     */
    constructor(num?: Number, name?: String, symbol?: String, occurrence?: Occurrence, orbital?: Orbital[], trend?: Trend, subtrend?: SubTrend, tChangeFase1?: Number, tChangeFase2?: Number, state?: State) {
		this._num = num;
		this._name = name;
		this._symbol = symbol;
		this._occurrence = occurrence;
		this._orbital = orbital;
		this._trend = trend;
		this._subtrend = subtrend;
		this._tChangeFase1 = tChangeFase1;
		this._tChangeFase2 = tChangeFase2;
		this._state = state;
	}

    /**
     * Getter num
     * @return {Number}
     */
	public get num(): Number {
		return this._num;
	}

    /**
     * Getter name
     * @return {String}
     */
	public get name(): String {
		return this._name;
	}

    /**
     * Getter symbol
     * @return {String}
     */
	public get symbol(): String {
		return this._symbol;
	}

    /**
     * Getter occurrence
     * @return {Occurrence}
     */
	public get occurrence(): Occurrence {
		return this._occurrence;
	}

    /**
     * Getter orbital
     * @return {Orbital[]}
     */
	public get orbital(): Orbital[] {
		return this._orbital;
	}

    /**
     * Getter trend
     * @return {Trend}
     */
	public get trend(): Trend {
		return this._trend;
	}

    /**
     * Getter subtrend
     * @return {SubTrend}
     */
	public get subtrend(): SubTrend {
		return this._subtrend;
	}

    /**
     * Getter tChangeFase1
     * @return {Number}
     */
	public get tChangeFase1(): Number {
		return this._tChangeFase1;
	}

    /**
     * Getter tChangeFase2
     * @return {Number}
     */
	public get tChangeFase2(): Number {
		return this._tChangeFase2;
	}

    /**
     * Getter state
     * @return {State}
     */
	public get state(): State {
		return this._state;
	}

    /**
     * Setter num
     * @param {Number} value
     */
	public set num(value: Number) {
		this._num = value;
	}

    /**
     * Setter name
     * @param {String} value
     */
	public set name(value: String) {
		this._name = value;
	}

    /**
     * Setter symbol
     * @param {String} value
     */
	public set symbol(value: String) {
		this._symbol = value;
	}

    /**
     * Setter occurrence
     * @param {Occurrence} value
     */
	public set occurrence(value: Occurrence) {
		this._occurrence = value;
	}

    /**
     * Setter orbital
     * @param {Orbital[]} value
     */
	public set orbital(value: Orbital[]) {
		this._orbital = value;
	}

    /**
     * Setter trend
     * @param {Trend} value
     */
	public set trend(value: Trend) {
		this._trend = value;
	}

    /**
     * Setter subtrend
     * @param {SubTrend} value
     */
	public set subtrend(value: SubTrend) {
		this._subtrend = value;
	}

    /**
     * Setter tChangeFase1
     * @param {Number} value
     */
	public set tChangeFase1(value: Number) {
		this._tChangeFase1 = value;
	}

    /**
     * Setter tChangeFase2
     * @param {Number} value
     */
	public set tChangeFase2(value: Number) {
		this._tChangeFase2 = value;
	}

    /**
     * Setter state
     * @param {State} value
     */
	public set state(value: State) {
		this._state = value;
	}

}