/*
  @name = Protein TS
  @author = David Medel / Esther Vendrell
  @version = 0.0.1
  @description = TypeScript of the Protein
  @date = 03-02-2020
*/

//imports
import { ComponentOfProtein } from './componentOfProtein';
import { TypeOfProtein } from './typeOfProtein';
import { FunctionModel } from './functionModel';

export class Protein {
    //properties
    private protein_name: String;
    private componentOfProtein: ComponentOfProtein;
    private typeOfProtein: TypeOfProtein;
    private functionOfProtein: FunctionModel[];

	/**
     *Creates an instance of Protein.
     * @param {String} [$protein_name]
     * @param {ComponentOfProtein} [$componentOfProtein]
     * @param {TypeOfProtein} [$typeOfProtein]
     * @param {FunctionModel[]} [$functionOfProtein]
     * @memberof Protein
     */
    constructor($protein_name?: String, $componentOfProtein?: ComponentOfProtein, $typeOfProtein?: TypeOfProtein, $functionOfProtein?: FunctionModel[]) {
		this.protein_name = $protein_name;
		this.componentOfProtein = $componentOfProtein;
		this.typeOfProtein = $typeOfProtein;
		this.functionOfProtein = $functionOfProtein;
	}

    /**
     * Getter $protein_name
     * @return {String}
     */
	public get $protein_name(): String {
		return this.protein_name;
	}

    /**
     * Getter $componentOfProtein
     * @return {ComponentOfProtein}
     */
	public get $componentOfProtein(): ComponentOfProtein {
		return this.componentOfProtein;
	}

    /**
     * Getter $typeOfProtein
     * @return {TypeOfProtein}
     */
	public get $typeOfProtein(): TypeOfProtein {
		return this.typeOfProtein;
	}

    /**
     * Getter $functionOfProtein
     * @return {FunctionOfProtein[]}
     */
	public get $functionOfProtein(): FunctionModel[] {
		return this.functionOfProtein;
	}

    /**
     * Setter $protein_name
     * @param {String} value
     */
	public set $protein_name(value: String) {
		this.protein_name = value;
	}

    /**
     * Setter $componentOfProtein
     * @param {ComponentOfProtein} value
     */
	public set $componentOfProtein(value: ComponentOfProtein) {
		this.componentOfProtein = value;
	}

    /**
     * Setter $typeOfProtein
     * @param {TypeOfProtein} value
     */
	public set $typeOfProtein(value: TypeOfProtein) {
		this.typeOfProtein = value;
	}

    /**
     * Setter $functionOfProtein
     * @param {FunctionOfProtein[]} value
     */
	public set $functionOfProtein(value: FunctionModel[]) {
		this.functionOfProtein = value;
	}

    /**
     * @name addRemoveFunctionProtein
     * @description function to control the check of checkbox; when we check some component or not. 
     */
    addRemoveFunctionProtein(FuncProt: FunctionModel): void{
    //If indexOf returns -1 means the object wasn't found
    //So it has to be added

    if(this.functionOfProtein.map(e => e.$id).indexOf(FuncProt.$id)==-1) {
        this.functionOfProtein.push(FuncProt);
    //Otherwise (indexOf returns != -1), the object was found and has to be removed
    } else {
        this.functionOfProtein.splice(this.functionOfProtein.map(e => e.$id).indexOf(FuncProt.$id), 1)
    }
  }

}