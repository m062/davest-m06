/*
  @name = ComponentOfProtein TS
  @author = David Medel / Esther Vendrell
  @version = 0.0.1
  @description = TypeScript of the ComponentOfProtein
  @date = 03-02-2020
*/

export class ComponentOfProtein {
    //properties
    private id: number;
    private component: string;

	/**
     *Creates an instance of ComponentOfProtein.
     * @param {number} $id
     * @param {string} $component
     * @memberof ComponentOfProtein
     */
    constructor($id: number, $component: string) {
		this.id = $id;
		this.component = $component;
	}

    /**
     * Getter $id
     * @return {number}
     */
	public get $id(): number {
		return this.id;
	}

    /**
     * Getter $component
     * @return {string}
     */
	public get $component(): string {
		return this.component;
	}

    /**
     * Setter $id
     * @param {number} value
     */
	public set $id(value: number) {
		this.id = value;
	}

    /**
     * Setter $component
     * @param {string} value
     */
	public set $component(value: string) {
		this.component = value;
	}

}