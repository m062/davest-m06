/*
  @name = FunctionModel TS
  @author = David Medel / Esther Vendrell
  @version = 0.0.1
  @description = TypeScript of the FunctionModel
  @date = 03-02-2020
*/

export class FunctionModel {
    //properties
    private id: number;
    private function: string;

	/**
     *Creates an instance of FunctionModel.
     * @param {number} $id
     * @param {string} $function
     * @memberof FunctionModel
     */
    constructor($id: number, $function: string) {
		this.id = $id;
		this.function = $function;
	}

    /**
     * Getter $id
     * @return {number}
     */
	public get $id(): number {
		return this.id;
	}

    /**
     * Getter $function
     * @return {string}
     */
	public get $function(): string {
		return this.function;
	}

    /**
     * Setter $id
     * @param {number} value
     */
	public set $id(value: number) {
		this.id = value;
	}

    /**
     * Setter $function
     * @param {string} value
     */
	public set $function(value: string) {
		this.function = value;
	}


}