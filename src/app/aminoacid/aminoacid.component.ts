/*
  @name = Aminoacid Component TS
  @author = David Medel / Esther Vendrell
  @version = 0.0.1
  @description = TypeScript of the Aminoacid component
  @date = 03-02-2020
*/

//imports
import { Component, OnInit, ViewChild } from '@angular/core';
import { Aminoacid } from '../model/aminoacid/Aminoacid';
import { Essentiality} from '../model/aminoacid/Essentiality';
import { ChainPropertie} from '../model/aminoacid/ChainPropertie';
import { FullInfoAmino} from '../model/aminoacid/FullInfoAmino';
//aminoacid Service
import { AminoacidService } from '../services/aminoacid/aminoacid.service';
//cookie
import { CookieService } from 'ngx-cookie-service';


//Component, selector tag...
@Component({
  selector: 'app-aminoacid',
  templateUrl: './aminoacid.component.html',
  styleUrls: ['./aminoacid.component.css']
})

export class AminoacidComponent implements OnInit {
  
  //asign the aminoacidForm to a variable
  @ViewChild('aminoacidForm', null) 
  aminoacidForm: HTMLFormElement;

  //properties
  objAminoacid: Aminoacid;

  //inicialize vars
  show_arr_aa: FullInfoAmino[]=[];
  essentiatilies: Essentiality[]=[];
  properties: ChainPropertie[]=[];
  fullInfoAminos: FullInfoAmino[]=[];
  //cookie object
  cookieObj: any;


  /**
   *Creates an instance of AminoacidComponent.
   * @param {CookieService} cookieService
   * @param {AminoacidService} aminoacidService
   * @memberof AminoacidComponent
   */
  constructor(
    private cookieService: CookieService,
    private aminoacidService: AminoacidService
  ) { }

  /**
   * @name ngOnInit
   * @description This method is a default method that is called when the page is loading
   * it calls some methods that inicialize and fills the properties,
   * and instance the objAtom and set his properties to default values
   */
  ngOnInit() {
    this.essentiatilies = this.aminoacidService.createEssentiality();
    this.properties = this.aminoacidService.createChainPropertie();
    this.fullInfoAminos = this.aminoacidService.createTableFullInfoAmino();
    
    this.objAminoacid = new Aminoacid();
    this.objAminoacid.essenciality = this.essentiatilies[0];
    this.objAminoacid.chainPropertie = this.properties[0];
    this.filterAmino();

    this.initializeForm();    
    this.getCookie();
  }

  /**
   * @name initializeForm
   * @description This method is for restart all the different params of the aminoacidForm. 
   */
  initializeForm(){
    this.aminoacidForm.reset();
    this.aminoacidForm.form.markAsPristine();
    this.aminoacidForm.form.markAsUntouched();

    //If this.objAminoacid is null is because
    //no input param has been received.
    //Otherwise, we should be careful not to
    //smash the parameter

    //if this.objAminoacid do not exist
    if(!this.objAminoacid){
      this.objAminoacid = new Aminoacid();

      this.objAminoacid.chainPropertie = this.properties[0];
      this.objAminoacid.essenciality = this.essentiatilies[0];
    }
  }

  /**
   *
   * @name aminoacid
   * @description Method that shows the objAminoacid and his properties in Console
   * And shows in console the show_arr_aa that contains the aminoacids that hace the selected properties
   */
   aminoacid(): void {
    this.cookieService.set("aminoacid", JSON.stringify(this.objAminoacid));
    console.log(this.objAminoacid);
    console.log(this.show_arr_aa);
  }

  /**
   *
   * @name filterAmino
   * @description Method that filters the aminoacid with the selected properties (essenciality and chainProperty)
   */
  filterAmino(): void{
    this.show_arr_aa =  this.fullInfoAminos.filter(aa => aa.essenciality.id === this.objAminoacid.essenciality.id);
    this.show_arr_aa =  this.show_arr_aa.filter(aa => aa.chainPropertie.id === this.objAminoacid.chainPropertie.id);
  }
  
  /**
   * @name getCookie
   * @description Method that gets all the cookies of aminoacid params.
   */
  getCookie() {
    if (this.cookieService.check("aminoacid")) {
      console.log("CookieAminoacid exists");
      
      this.cookieObj = JSON.parse(this.cookieService.get("aminoacid"));
      
      Object.assign(this.objAminoacid, this.cookieObj);

      this.objAminoacid.chainPropertie = this.properties[this.cookieObj._chainPropertie._id];
      this.objAminoacid.essenciality = this.essentiatilies[this.cookieObj._essenciality._id];

      //for the table -> to be accord with the cookie params
      this.show_arr_aa =  this.fullInfoAminos.filter(aa => aa.essenciality.id === this.objAminoacid.essenciality.id);
      this.show_arr_aa =  this.show_arr_aa.filter(aa => aa.chainPropertie.id === this.objAminoacid.chainPropertie.id);
    }
  }
}
